package pl.sdacademy.vetclinic.qualifier;

import javax.inject.Qualifier;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Qualifier //uwaga na importy -> musi byc javax.inject.Qualifier
@Retention(RetentionPolicy.RUNTIME) //adnotacje te sa grupowane do tej jednej ..
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface DefaultRepository {
}
