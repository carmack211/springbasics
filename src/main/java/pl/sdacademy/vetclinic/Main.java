package pl.sdacademy.vetclinic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class Main {

	public Main() {

	}

	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
	}


}
