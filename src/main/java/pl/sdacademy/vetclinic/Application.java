package pl.sdacademy.vetclinic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sdacademy.vetclinic.repository.AppointmentRepository;
import pl.sdacademy.vetclinic.repository.DoctorRepository;
import pl.sdacademy.vetclinic.repository.PetOwnerRepository;
import pl.sdacademy.vetclinic.repository.PetRepository;
import pl.sdacademy.vetclinic.repository.RoomRepository;
import pl.sdacademy.vetclinic.repository.StaffRepository;
import pl.sdacademy.vetclinic.repository.impl.*;
import pl.sdacademy.vetclinic.service.AppointmentService;
import pl.sdacademy.vetclinic.service.PropertySampleService;
import pl.sdacademy.vetclinic.service.SummaryService;

import javax.inject.Inject;

@Service("application")
public class Application {

	@Inject
	private SummaryService summaryService;
	@Inject
	private PropertySampleService propertySampleService;
	@Inject
	private DatabaseStaffRepository databaseStaffRepository;

	public void setPropertySampleService(PropertySampleService propertySampleService) {
		this.propertySampleService = propertySampleService;
	}

	public void printGeneralSummary() {
		System.out.println(summaryService.getOverallSummary());
		System.out.println(propertySampleService.toString());
		System.out.println(databaseStaffRepository);

	}

	public SummaryService getSummaryService() { return summaryService; }

	public void setSummaryService(SummaryService summaryService) { this.summaryService = summaryService; }
}
