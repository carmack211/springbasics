package pl.sdacademy.vetclinic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PropertySampleService {
    @Value("${db.username}")
    private String username;
    @Value("${db.password}")
    private String property1;
    @Value("${db.adress}")
    private String property2;


    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getProperty1() { return property1; }

    public void setProperty1(String property1) { this.property1 = property1; }

    public String getProperty2() { return property2; }

    public void setProperty2(String property2) { this.property2 = property2; }

    @Override
    public String toString() {
        return "PropertySampleService{" +
                "username='" + username + '\'' +
                ", property1='" + property1 + '\'' +
                ", property2='" + property2 + '\'' +
                '}';
    }
}
