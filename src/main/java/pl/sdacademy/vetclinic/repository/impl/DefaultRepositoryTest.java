package pl.sdacademy.vetclinic.repository.impl;

import org.springframework.stereotype.Repository;
import pl.sdacademy.vetclinic.model.Pet;
import pl.sdacademy.vetclinic.qualifier.DefaultRepository;
import pl.sdacademy.vetclinic.repository.PetRepository;

import java.util.List;

@Repository
@DefaultRepository
public class DefaultRepositoryTest implements PetRepository {

    @Override
    public Pet getById(Integer petId) {
        return null;
    }

    @Override
    public void add(Pet pet, Integer ownerId) {

    }

    @Override
    public List<Pet> getByOwner(Integer ownerId) {
        return null;
    }

    @Override
    public Long count() {
        return null;
    }
}
