package pl.sdacademy.vetclinic.repository.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import pl.sdacademy.vetclinic.model.Staff;
import pl.sdacademy.vetclinic.qualifier.DatabaseRepository;
import pl.sdacademy.vetclinic.repository.StaffRepository;

import javax.annotation.PostConstruct;
import java.util.List;

@Repository
@DatabaseRepository
public class DatabaseStaffRepository implements StaffRepository
{

    @Value("${db.username}")
    private String username;
    @Value("${db.password}")
    private String password;
    @Value("${db.adress}")
    private String databaseAdress;
    @Value("${db.name}")
    private String databaseName;

    @Override
    public void add(Staff staff) {
    }

    @Override
    public Staff getById(Integer staffId) {
        return null;
    }

    @Override
    public List<Staff> getAll() {
        return null;
    }

    @Override
    public Long count() {
        return null;
    }

    @PostConstruct
    public void printInjectedProperties(){
        System.out.printf("username: " + username);
        System.out.printf("password: " + password);
        System.out.printf("databaseAdress: " + databaseAdress);
        System.out.printf("databaseName: " + databaseName);
    }

    @Override
    public String toString() {
        return "DatabaseStaffRepository{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", databaseAdress='" + databaseAdress + '\'' +
                ", databaseName='" + databaseName + '\'' +
                '}';
    }
}
