package pl.sdacademy.vetclinic.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.vetclinic.model.Room;
import pl.sdacademy.vetclinic.repository.RoomRepository;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping(value = "api/",
        produces = MediaType.APPLICATION_JSON_VALUE) //do zmapowania adresu
public class RoomController {

    @Inject
    private RoomRepository roomRepository;

    @RequestMapping(value ="rooms", method = RequestMethod.GET)
    public List<Room> getAll(){
        return roomRepository.getAll();
    }

    @RequestMapping(value = "rooms/{id}", method = RequestMethod.GET)
    public Room getRoom(@PathVariable("id") Integer id) {
        return roomRepository.getById(id);
    }

    @RequestMapping(value = "rooms", method = RequestMethod.POST)
    public void addRoom(@RequestBody Room receivedRoom) {
        roomRepository.add(receivedRoom);
    }

    @RequestMapping(value = "rooms/{id}", method = RequestMethod.DELETE)
    public Room deleteRoom(@PathVariable("id") Integer id) {
        return roomRepository.remove(id);
    }

}
